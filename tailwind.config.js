/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './public/**/*.html',
    './src/**/*.{js,jsx,ts,tsx,vue}'
  ],
  mode: 'jit',
  darkMode: 'class',
  theme: {
    extend: {
      colors: {
        transparent: 'transparent',
        dark: '#292929',
        darkb: '#666666',
        brgreen: '#008442',
        bryellow: '#FCC72E',
        aad: '#01BDF3'
      }
    },
  },
  plugins: [],
}

