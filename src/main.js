import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import FlagIcon from 'vue-flag-icon'

// import VueGoogleMaps from '@fawmi/vue-google-maps'

import App from './App.vue'
import router from './router'

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(FlagIcon);
// app.use(VueGoogleMaps, {
//     load: {
//         key: 'AIzaSyCXQV4FpKYoFva5dmQkqTBFgZ2Ce81I204',
//     },
// })

app.mount('#app')
