import { defineStore } from 'pinia'

export const entityStore = defineStore('entity', {
    state: () => {
        return { 
            entitys: [
            ],
            nearbyEntitys:[],
            nearbyDisplayFlag: false,
            entitysSelected:[],
            entityDisplay: {},
            entityDisplayFlag: false,
            vesselPath: null,
            cleanMap: null,
            cleanQuery: false
        }
    },
    getters: {
        getEntitys: (state)=>state.entitysSelected,
        getEntity: (state)=> (vessel_name) => {
            return state.entitys.filter(v => v.vessel === vessel_name)[0];
        },
        getDisplayed: (state)=>state.entityDisplayFlag,
        getNearbyEntitys: (state)=>state.nearbyEntitys,
        getNearbyDisplayFlag: (state)=>state.nearbyDisplayFlag,
        getEntityDisplayed: (state)=>state.entityDisplay,
        getCleanMap: (state)=>state.cleanMap
    },
    actions: {
        setEntitys(data) {
            this.entitys = data
        },
        setNearbyEntitys(data) {
            this.nearbyEntitys = data
        },
        setCleanMap(data) {
            this.cleanMap = data
        },
        setCleanQuery(data) {
            this.cleanQuery = data
        },
        setVesselPath(data) {
            this.vesselPath = data
        },
        setSelecteds(searchQuery) {
            if (searchQuery.length == 0){
                this.entitysSelected = []    
            } else{
                this.entitysSelected = this.entitys.filter(
                    v=> v.vessel.toLocaleLowerCase().includes(searchQuery.toLowerCase())
                )
            }
        },
        setNearbyDisplayFlag(data) {
            this.nearbyDisplayFlag = !data
        },
        setEntityDisplay(vessel_name, flag, type) {
            if (flag){
                console.log(type)
                if (type == 'fleet') {
                    this.entityDisplay = this.entitys.filter(
                        v=> v.vessel.toLocaleLowerCase().includes(vessel_name.toLowerCase())
                    )[0]
                } else {
                    this.entityDisplay = this.nearbyEntitys.filter(
                        v=> v.vessel.toLocaleLowerCase().includes(vessel_name.toLowerCase())
                    )[0]
                }
                this.entityDisplayFlag = flag
            } else {
                this.entityDisplay = {}
                this.entityDisplayFlag = false
            }
            
        }
    }
});