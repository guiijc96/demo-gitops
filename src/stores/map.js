import { defineStore } from 'pinia'
import { toRaw } from 'vue'
import { setMapZoom } from '@/composables/zoom'

export const useMapStore = defineStore('map', {
    state: () => {
        return { 
            lines: [
            ],
            cleanMap: null,
            centerMap: null,
            zoomMap: null,
            darkMode: false,
            mapProxy: null
        }
    },
    getters: {
        getLines: (state)=>state.lines,
        getCleanMap: (state)=>state.cleanMap,
        getMap: (state)=>toRaw(state.mapProxy)
    },
    actions: {
        setLines(data) {
            if (data.length > 0){
                data.forEach(element => {
                    this.lines.push(element)    
                });
            } else {
                for (let index = 0; index < this.lines.length; index++) {
                    this.lines.pop()    
                }
            }
            
        },
        setDarkMode() {
            console.log('teste')
            this.darkMode = !this.darkMode
        },
        setCleanMap(data) {
            this.cleanMap = data
        },
        setCenterMap(data) {
            this.centerMap = data
        },
        cleanMap() {
            if(this.lines.length>0){
                this.lines.forEach(line => {
                    toRaw(line).setMap(null);
                });
                this.setLines([]);
                this.cleanMap = false;
            }
            
        },
        setMap(instance) {
            this.mapProxy = instance
        },
        setZoom(options) {
            toRaw(this.mapProxy).setOptions(options)
        },
        createPath(PathCoordinates){
            const line = new google.maps.Polyline({
                path: PathCoordinates,
                geodesic: true,
                strokeColor: '#FF0000',
                strokeOpacity: 1.0,
                strokeWeight: 2,
                map: toRaw(this.mapProxy),
              });
              
              const zPath = setMapZoom(PathCoordinates);
              const bounds = new google.maps.LatLngBounds(
                new google.maps.LatLng(zPath.minLat, zPath.minLng),
                new google.maps.LatLng(zPath.maxLat, zPath.maxLng)
              );
              map.value.fitBounds(bounds);
              map.value.setZoom(zPath.calculatedZoom);
              console.log(zPath.calculatedZoom)
              this.lines.push(line)
        }
    }
});