import { defineStore } from 'pinia'
import { usePortStore } from '@/stores/port'
import { useVesselStore } from '@/stores/vessel'
import { useMapStore } from '@/stores/map'
import { reactive, ref, toRaw } from 'vue'

export const useEntities = defineStore('entities', () => {
    const portStore = usePortStore()
    const vesselStore = useVesselStore()
    const mapStore = useMapStore()
    const entitiesQueries = ref([])
    const cleanQuery = ref(false)
    const displayed = (name, flag) => { vesselStore.setDisplayed() }
    const signalFleet = () => { vesselStore.signalFleet() }
    const showingNearby = (Map, flag) => { vesselStore.showingNearby(Map, true) }
    const cleanMap = () => { portStore.hiddenPort(); vesselStore.cleanMap() }

    function setCleanQuery(){
        cleanQuery.value = !cleanQuery.value
        getFromQuery('')
    }
    
    function showEntityOnly(entityInstance,type){
        // if(type!=='port'){
            toRaw(entityInstance.marker_point).setMap(toRaw(mapStore.getMap))
        // }
    }    

    function generateMarker(Map, marker, type) {
        let Marker = toRaw(marker)
        
        
        if (type === 'port'){
            portStore.getPorts.forEach(element => {
                element.marker_point = new Marker({
                    position: {lat: element.latitude, lng:element.longitude},
                    map: toRaw(Map),
                    title: element.name,
                    label: '',
                })
                // toRaw(element.marker_point).addListener("mouseover", () => {
                //     var infowindow = new google.maps.InfoWindow({
                //         content: marker.title
                //     })
                //     infowindow.setContent(marker.title+'123123');
                // });
            });
        }
        if (type === 'fleet'){
            vesselStore.getFleet.forEach(element => {
                element.marker_point = new Marker({
                    position: {lat: element.latitude, lng:element.longitude},
                    map: toRaw(Map),
                    title: element.name,
                    label: {
                        text: element.name,
                        className: 'marker-position',
                        fontSize: "10px",
                        color: "#777",
                        fontFamily: 'Verdana'
                    },
                    icon: {
                        url: '/online.svg',
                        scaledSize: new google.maps.Size(20, 20)
                    }
                })
                toRaw(element.marker_point).addListener("click", () => {
                    vesselStore.setDisplayed(toRaw(element.marker_point).title, true, type)
                });
            });
        }
        if (type === 'nearby'){
            vesselStore.getNearby.forEach(element => {
                element.marker_point = new Marker({
                    position: {lat: element.latitude, lng:element.longitude},
                    map: toRaw(Map),
                    title: element.name,
                    label: '',
                    icon: {
                        path: "M0.5,31.68c0.001-2.656,0.01-15.152,0.029-19.642C0.541,9.454,4.853,0.68,7.472,0.68c2.589,0.016,6.947,8.825,6.989,11.412c0.071,4.446,0.016,16.937,0.003,19.588H0.5z",
                        fillColor: "#dededd",
                        strokeWeight: 2,
                        fillOpacity: 1,
                        // rotation: element.heading!==null?heading:0,
                        scale: 0.7
                    }
                })
                toRaw(element.marker_point).addListener("click", () => {
                    vesselStore.setDisplayed(toRaw(element.marker_point).title, true, type)
                });
                // toRaw(element.marker_point).addListener("mouseover", () => {
                //     var infowindow = new google.maps.InfoWindow({
                //         content: marker.title
                //     })
                //     infowindow.setContent(marker.title);
                // });
            });
        }
    }

    function getEntity(type, name) {
        let entity;
        switch (type) {
            case 'vessel':
                entity = vesselStore.getVessel(name)
                break;
            case 'port':
                entity = portStore.getPort(name)
                break;
        }
        return entity
    }

    function getFromQuery(query) {
        if (query !== ''){
            let arr = []
            var ports = portStore.getPortQuery(query);
            var vessel = vesselStore.getQuery(query);
            arr = arr.concat(ports).concat(vessel)
            if (arr.length > 0) {
                entitiesQueries.value = arr
            } else {
                entitiesQueries.value = []
            }
        } else {
            entitiesQueries.value = []
        }
    }

    function setEntities(data) {
        portStore.setPorts(
            data.ports.map(object => {
                return {...object, type: 'port', marker_point:null};
            })
        )
        vesselStore.setNearby(
            data.aisVessels.map(object => {
                return {...object, type: 'vessel', marker_point:null};
            })
        )
        vesselStore.setFleet(
            data.vessels.map(object => {
                return {...object, type: 'vessel', marker_point:null};
            })
        )
    }

    return {
        portStore
        , entitiesQueries
        , cleanQuery
        , cleanMap
        , displayed
        , signalFleet
        , showingNearby
        , showEntityOnly
        , generateMarker
        , setEntities
        , setCleanQuery
        , getFromQuery
        , getEntity
    }
})