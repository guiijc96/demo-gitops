import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useAuth = defineStore('auth', () => {
  const token = ref(localStorage.getItem('token'))
  const user = ref(localStorage.getItem('user'))

  function setToken(tokenValue) {
    localStorage.setItem('token', tokenValue);
    token.value = tokenValue;
  }
  function setUser(userValue) {
    localStorage.setItem('user', userValue);
    user.value = userValue;
  }
  function checkToken(token) {
    // if (token === '123') {
      return true
    // } else {
      return false
    // }

  }

  return { token, user, setUser, setToken, checkToken }
})
