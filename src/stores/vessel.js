import { defineStore } from 'pinia'
import { ref, reactive, toRaw } from 'vue'

export const useVesselStore = defineStore('vessel', {
    state: () => {
        return { 
            fleet: reactive([]),
            nearby: reactive([]),
            clustersNearby: reactive([]),
            displayed: ref([])
        }
    },
    getters: {
        getAll: (state)=>state.fleet.concat(state.nearby),
        getFleet: (state)=>state.fleet,
        getNearby: (state)=>state.nearby,
        getDisplayed: (state)=>{ return state.displayed},
        getVessel: (state)=> (name) => {
            let fl = state.fleet.filter(v => v.name === name)[0];
            let nb = state.nearby.filter(v => v.name === name)[0];
            if (fl !== undefined) {
                return fl
            } 
            if (nb !== undefined) {
                return nb
            }
        },
        getFleetVessel: (state)=> (name) => {
            return state.fleet.filter(v => v.vessel === name)[0];
        },
        getNearbyVessel: (state)=> (name) => {
            return state.fleet.filter(v => v.name === name)[0];
        },
        getQuery: (state)=> (searchQuery) => {
            // console.log(state.fleet)
            // console.log(state.nearby)
            let fl = state.fleet.filter(v => v.vessel.toLocaleLowerCase().includes(searchQuery.toLowerCase()));
            let nb = state.nearby.filter(v => v.vessel.toLocaleLowerCase().includes(searchQuery.toLowerCase()));
            // console.log(fl)
            return fl.concat(nb)
        }
    },
    actions: {
        setFleet(data) {
            this.fleet = data
        },
        setNearby(data) {
            this.nearby = data
        },
        cleanMap(){
            this.nearby.forEach(vessel => {
                toRaw(vessel.marker_point).setMap(null)
            })
        },
        signalFleet(){
            this.fleet.forEach(fleet => {
                try {
                    let actualCommitVessel = new Date(this.getFleetVessel(fleet.marker_point.title).commit)-(3*60*60*1000)
                    let compNowDate = Date.now() - (15*60*1000)
                    if(actualCommitVessel < compNowDate){
                        toRaw(fleet.marker_point).setIcon({
                            url: '/offline.svg',
                            scaledSize: new google.maps.Size(20, 20),
                        })
                    }  
                } catch (error) {
                    // console.log(error)
                    toRaw(fleet.marker_point).setIcon({
                        url: '/offline.svg',
                        scaledSize: new google.maps.Size(20, 20),
                    })
                }
            });
        },
        showingNearby(Map, flag){
            this.getNearby.forEach(fleet => {
                if (flag){
                    toRaw(fleet.marker_point).setMap(toRaw(Map))
                } else {
                    toRaw(fleet.marker_point).setMap(null)
                }
            });
        },
        setDisplayed(name, flag, type){
            if (flag){
                let disp = [this.getVessel(name)]
                
                this.displayed.value = disp.map(el=>{
                    return {
                        id: el.id,
                        date: el.date,
                        latitude: el.latitude,
                        longitude: el.longitude,
                        name: el.name,
                        type: el.type,
                        vessel: el.vessel
                    }
                })
            } else {
                this.displayed.value = false
            }
        }
    }
});