import { defineStore } from 'pinia'
import { ref, toRaw } from 'vue'

export const usePortStore = defineStore('port', {
    state: () => {
        return { 
            ports: [
            ]
        }
    },
    getters: {
        getPorts: (state)=>state.ports,
        getPort: (state)=> (port_name) => {
            return state.ports.filter(v => v.name === port_name)[0];
        },
        getPortQuery: (state)=> (searchQuery) => {
            // console.log(state.ports)
            return state.ports.filter(v => v.name.toLocaleLowerCase().includes(searchQuery.toLowerCase()));
        }
    },
    actions: {
        setPorts(data) {
            this.ports = data
        },
        hiddenPort(){
            this.ports.forEach(port=>{
                toRaw(port.marker_point).setMap(null)
            })
        }
    }
});