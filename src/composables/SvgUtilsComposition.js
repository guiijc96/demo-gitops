// SvgUtilsComposition.js
import axios from 'axios';
// Função para girar o SVG
function rotateSVG(svg, rotation) {
    const wrapper = document.createElement('div');
    wrapper.innerHTML = svg;
    const svgElement = wrapper.querySelector('svg');
    
    svgElement.setAttribute('transform', `rotate(${rotation})`);
    return svgElement.outerHTML;
  }
  
  // Função para criar um Blob a partir de um SVG
  function createSvgBlob(svg) {
    const svgBlob = new Blob([svg], { type: 'image/svg+xml;charset=utf-8' });
    return URL.createObjectURL(svgBlob);
  }
  
  async function createSvgBlobFromPath(svgPath, rotation) {
    try {
      const response = await axios.get(svgPath);
      const rotatedSvg = rotateSVG(response.data, rotation);
      const svgBlob = new Blob([rotatedSvg], { type: 'image/svg+xml;charset=utf-8' });
      return URL.createObjectURL(svgBlob);
    } catch (error) {
      console.error('Erro ao carregar o arquivo SVG:', error);
      return null;
    }
  }

  // Exporta as funções
  export {
    rotateSVG,
    createSvgBlob,
    createSvgBlobFromPath
  };