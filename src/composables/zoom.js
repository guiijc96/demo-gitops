function zoom(mapPx, worldPx, fraction) {
    return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
}

function latRad(lat) {
    const sin = Math.sin((lat * Math.PI) / 180);
    const radY = Math.log((1 + sin) / (1 - sin)) / 2;
    return Math.max(Math.min(radY, Math.PI), -Math.PI) / 2;
}

function calculateZoom(latDiff, lngDiff) {
    const WORLD_DIM = { height: 256, width: 256 };
    const ZOOM_MAX = 10;
    const latFraction = (latRad(maxLat) - latRad(minLat)) / Math.PI;
    const latZoom = zoom(WORLD_DIM.height, 256, latFraction);
    const lngZoom = zoom(WORLD_DIM.width, 256, lngDiff / 360);

    return Math.min(latZoom, lngZoom, ZOOM_MAX);
}

function setMapZoom(lines) {
    const latitudes = lines.map(point => point.lat);
    const longitudes = lines.map(point => point.lng);

    const minLat = Math.min(...latitudes);
    const maxLat = Math.max(...latitudes);
    const minLng = Math.min(...longitudes);
    const maxLng = Math.max(...longitudes);

    const latDiff = maxLat - minLat;
    const lngDiff = maxLng - minLng;

    const calculatedZoom = calculateZoom(latDiff, lngDiff);
    return {
      calculatedZoom:calculatedZoom, 
      minLat:minLat, 
      minLng:minLng, 
      maxLat:maxLat, 
      maxLng:maxLng
    }
}

export {
    setMapZoom
}