import axios from 'axios';

const axiosInstance = axios.create({
    baseURL: 'https://ibus.transpetro.com.br/safe-ds/dev',
    headers: {
        'Content-Type': 'aplication/json'
    }
});
export default axiosInstance;