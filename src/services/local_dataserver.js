import axios from 'axios';

const axiosInstance = axios.create({
    baseURL: 'http://localhost:80',
    headers: {
        'Content-Type': 'aplication/json'
    }
});
export default axiosInstance;